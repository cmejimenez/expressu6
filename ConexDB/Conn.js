const mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1:27017/candystore");
const miconexion = mongoose.connection;
miconexion.on("connected", () => {
    console.log("Conexion exitosa a la base de datos MongoDB!!!");
});
miconexion.on("error", () => {
    console.log("Hay un error en la conexion a MongoDB!!!");
});
module.exports = mongoose;